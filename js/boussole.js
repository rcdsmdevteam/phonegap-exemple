/**
 * Created by orion on 20/03/15.
 */

/*
alert('toto');

var a = 15;
var s = 'toto';
var f = function(arg1, arg2){};
*/

//on déplace le code pour qu'il soit éxécuté une fois que la page est totalement chargée
document.addEventListener('deviceReady', function(){

/*
    //analogie avec java new JQuery('body')
    $('body').click(function(){
        alert('click sur body');
    });
    $('disk').click(function(){
        alert('click sur disk');
    });
*/
/*
    $('#disk').click(function(){
        $(this).css('transform', 'rotate('+365*Math.random()+'deg)');
    });
*/

    var watchID = navigator.compass.watchHeading(function(direction){
        $('#disk').css('transform', 'rotate('+direction.trueHeading+'deg)');
    }, function(){
        alert('error : heading not found');
    });

}, false);